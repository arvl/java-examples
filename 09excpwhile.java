import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;

public class excpwhile {
    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        int tries = 3;
        while ( tries > 0 ) {
            try {
                System.out.print("Enter anything: ");
                int userResponse = userInput.nextInt();
                switch (userResponse) {
                    case 420:
                        throw new InputMismatchException();
                    default:
                        System.out.print(userResponse + "\n");
                }
                tries = tries - 1;
                continue;
            }
            catch (InputMismatchException e) {
                // InputMismatchException() is thrown
                // by nextInt() which doesn't take
                // newline (\n) characters.
                // So we put a nextLine() here to prevent
                // the next nextLine() from receiving the
                // leftover newline character.
                userInput.nextLine();
                System.out.print("OwO what's this\n");
                continue;
            }
            catch (NoSuchElementException e) {
                System.out.print("UwU Goodbye.\n");
                System.exit(0);
            }
        }
 
    }
}
