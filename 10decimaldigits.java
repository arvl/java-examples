public class decimaldigits {
    public static void main(String[] args) {

        double firstNumber;
        double secondNumber;
        double answer;
        
        firstNumber = 10;
        secondNumber = 3;

        // Numbers without a decimal are treated like
        // integers when dividing (even though the
        // data type used to initialize it wasn't an
        // integer).
        // So we specify the correct datatype beside
        // it to let the compiler know we really want
        // that data type.
        answer = (double)firstNumber / secondNumber;

        System.out.print(firstNumber + " divided by " + secondNumber + " is " + answer + ".\n");

    }
}
