public class bubblesort {
    public static void main(String[] args) {

        int[] numbers = { 30013, 22073, 28545, 1522, 9440, 27814, 9428, 9048, 26406, 6254 };
        int[] numbers_inc = new int[numbers.length];
        int[] numbers_dec = new int[numbers.length];

        System.arraycopy( numbers, 0, numbers_inc, 0, numbers.length );
        System.arraycopy( numbers, 0, numbers_dec, 0, numbers.length );

        System.out.print("Number set: ");
        for ( int i = 0; i < numbers.length; i++ ) {
            System.out.printf(" %d", numbers[i]);
        }
        System.out.print("\n");

        while (true) {
            int swaps = 0;
            for ( int i = 0; i < numbers_inc.length - 1; i++ ) {
                int curr = numbers_inc[i];
                int next = numbers_inc[i+1];
                if ( curr > next ) {
                    numbers_inc[i] = next;
                    numbers_inc[i+1] = curr;
                    swaps++;
                }
            }
            if ( swaps == 0 ) {
                break;
            }
        }

        while (true) {
            int swaps = 0;
            for ( int i = 0; i < numbers_dec.length - 1; i++ ) {
                int curr = numbers_dec[i];
                int next = numbers_dec[i+1];
                if ( curr < next ) {
                    numbers_dec[i] = next;
                    numbers_dec[i+1] = curr;
                    swaps++;
                }
            }
            if ( swaps == 0 ) {
                break;
            }
        }

        System.out.print("Decreasing: ");
        for ( int i = 0; i < numbers_inc.length; i++ ) {
            System.out.printf(" %d", numbers_inc[i]);
        }
        System.out.print("\n");

        System.out.print("Increasing: ");
        for ( int i = 0; i < numbers_dec.length; i++ ) {
            System.out.printf(" %d", numbers_dec[i]);
        }
        System.out.print("\n");

    }
}
