import java.util.Scanner;

public class printtext {
    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);
        String name;
        int number;

        System.out.print("\n\tH\tE\tH\tE\tH\n");
        System.out.print("\tHello, World!\n\n"); 

        System.out.print("Please give me a number: ");
        number = userInput.nextInt();
        // nextInt() takes only the number given
        // to it, and excludes the newline (\n)
        // character.
        // Hence, we put a nextLine() here to prevent
        // the next nextLine() from receving it.
        userInput.nextLine();

        System.out.print("Now, tell me your name: ");
        name = userInput.nextLine();

        System.out.print("\n");

        System.out.print("Hello, " + name + "!\n");
        System.out.print("Your number is " + number + ".\n");

    }
}
